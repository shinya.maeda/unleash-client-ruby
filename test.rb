require 'unleash'
require 'unleash/context'

# environment_name = 'production'
# environment_name = 'staging'
environment_name = 'review/feature-1'

Unleash.configure do |config|
  config.url      = 'http://localhost:8181/api/v4/feature_flags/unleash/19'
  config.app_name = environment_name
  config.custom_http_headers = {
      'UNLEASH-INSTANCEID' => 'bp4Y_-BxFAtj4y-A2Zy8', # Set instance id
      'UNLEASH-APPNAME' => environment_name # Set App name 
  }
end

@unleash = Unleash::Client.new(
  url: 'http://localhost:8181/api/v4/feature_flags/unleash/19',
  instance_id: 'bp4Y_-BxFAtj4y-A2Zy8',
  app_name: environment_name)

feature_name = "ci_live_trace"
unleash_context = Unleash::Context.new
unleash_context.user_id = 123

if @unleash.is_enabled?(feature_name, unleash_context)
  puts " #{feature_name} is enabled according to unleash"
else
  puts " #{feature_name} is disabled according to unleash"
end
